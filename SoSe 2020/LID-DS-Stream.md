# LID-DS als Stream

## Problemstellung
In Host Intrusion Detection Systemen fallen enorme Mengen an Daten pro Host und Sekunde an.
Diese sollen "so schnell es geht" auf (mindestens) einem anderen Host ausgewertet werden.
Die Daten müssen also schnell es geht und ohne Datenverlust zu den auswertenden Host gelangen.
Die Idee ist dazu Kafka als verteilten Messagebroker und Protocol Buffers zum Serialisieren der Daten zu nutzen.

## Aufgabenstellung
* baut ein System, welches Daten aus dem LID-DS Datensatz liest
* jeder lesende Prozess schreibt dann die von ihm gelesenen Daten in eine Kafka Queue
* dann gibt es noch die verarbeitenden Prozesse, diese müssen die Daten aus Kafka lesen und einfache Statistiken darauf berechnen

![overview](lid-ds-stream/lid-ds-stream.png "overview")

* die Herausforderung besteht darin bei gleicher Hardware möglichst schnell den kompletten Datensatz zu verarbeiten
* Was sind die bottlenecks?
* Welche Varianten sind möglich?
* Sind die Technologien Kafka und Protocol Buffers die richtigen?
* ...
* alternativ können auf den Producern auch live Daten generiert werden mit sysdig - dann ist die Fragestellung: wie können wir den Durchsatz maximieren?


## Datensätze
LID-DS (Leipzig Intrusion Detection - Data Set)
* https://www.exploids.de/lid-ds/

## Links
* Protocol Buffers: https://developers.google.com/protocol-buffers
* Apache Kafka: https://kafka.apache.org/
