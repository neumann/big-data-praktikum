# Temporal Graph Explorer

## Motivation

Graphen sind ein intuitives Datenmodell um komplexe Beziehungen zwischen Entitäten aus realen 
Szenarios zu modellieren und zu analysieren. Da sich diese Objekte und Beziehungen in der Realität 
über die Zeit verändern, verändern sich auch die modellierten Graphen bezogen auf deren 
Struktur und Inhalt. Beispielsweise kommen neue Knoten und Kanten hinzu, werden entfernt oder 
verändern sich hinsichtlich ihrer Attribute.

Die Analyse dieser zeitlichen Veränderung von Graphen ist ein wichtiger Bestandteil, um genauere 
Informationen zum Entstehungsprozess von enthaltenen Strukturen zu erfahren oder herauszufinden, wann
eine Beziehung erstmalig auftrat oder wie lange im Durchschnitt eine Beziehung gültig ist.

Das an der Abteilung Datenbanken entwickelte Graph-Analyse-System Gradoop ist ein verteiltes Framework
zur Analyse solcher sogenannter temporaler Graphen. Das System
basiert auf Apache Flink, einem Open Source Stream Processing Framework. Es beinhaltet verschiedene 
Operatoren, die innerhalb einer Anwendung kombiniert werden können, um komplexere Workflows zu erzeugen.

Da Gradoop selbst keine Oberfläche zur Konfiguration der Operatoren und Anzeige von Graphen besitzt,
soll ein "Temporal Graph Explorer" entwickelt werden. Dieser 
könnte beispielsweise wie folgt aussehen: 

![Temporal graph Explorer](temporal-graph-explorer/temporal-gaph-explorer.png "Temporal Graph Explorer")

## Zielstellung

Ziel des Themas ist die Entwicklung einer Web-basierten Anwendung namens __Temporal Graph Explorer__
 zur Konfiguration der folgenden temporalen Operatoren:

* __Snapshot__: Die Extraktion eines Graph-Snapshots durch Angabe eines Zeitpunkts oder -intervals.
* __Difference__: Der Vergleich von zwei Graph-Snapshots.
* __Grouping__: Die Zusammenfassung eines Graphen durch Gruppierung und Aggregation der Knoten und Kanten.

Das Ergebnis eines jeden Operators ist wiederum ein temporaler Graph welcher visualisiert werden soll. Um die
zeitliche Entwicklung des Graphen hervorzuheben, soll eine geeingete Färbung der Knoten und Kanten
verwendet werden (z. B. rot (=älter) --> grün (=jünger)).

Ein Beispielgraph wird bereitgestellt. Er enthält Daten über Fahrrad-Ausleihen von CitiBike-Stationen 
von New York City, wobei Stationen über Knoten und Trips über Kanten modelliert werden.

Weiterhin wird die Anwendung "Gradoop Demo" (open-source auf github) bereitgestellt. Diese soll als 
Basis für die Implementierung des "Temporal Graph Explorer" verwendet werden. Dort sind bereits zwei
Operatoren vollständig implementiert. In Gradoop ist jeder Operator per Java-API parametrisierbar. 
Diese möglichen Parameter sollen über die Weboberfläche konfiguriert werden können. Folgende Abbildung
zeigt den aktuellen Stand der Gradoop Demo. Dort sieht man eine Möglichkeit der Konfiguration des
Grouping Operators (ohne temporale Eigenschaften).

![Gradoop demo](temporal-graph-explorer/gradoop-demo.png "Gradoop Demo")



__Zusatz__

Da die CitiBike Stationen geographische Informationen (lat, lon) enthalten, bietet es sich an, bei 
der Visualisierung des Graphen ein Karten-Layout zu verwenden. Die nachfolgende Abbildung zeigt, wie 
dies beispielsweise aussehen könnte (Kanten sind ausgeblendet).

![Map Layout](temporal-graph-explorer/map.png "Map Layout")



## Arbeitspakete

__1 - Technlogieauswahl und Lösungsskizze__

Zur Lösung des Problem ist es notwendig sich mit Gradoop sowie dessen Datenmodell (TPGM) und 
den drei Operatoren _snapshot_, _diff_ und _groupBy_ vertraut zu machen. 
Darüber hinaus sollte die Funktionsweise der Gradoop Demo 
untersucht werden. Die darin verwendete Library zur Visualisierung des Graphen sollte mit anderen 
existierenden Libraries zur Darstellung von Netzwerken verglichen werden. Wenn eine geeignetere 
Technologie gefunden wird, kann diese verwendet werden.

Es ist eine Lösungsskizze anzufertigen, die beschreibt, wie die Interaktion zwischen Frontend, 
Web-Service und Backend für die drei neuen Operatoren realisiert werden soll. Weiterhin sind drei 
Skizzen für die Gestaltung der jeweiligen Konfigurationsoberfläche anzufertigen.

__2 - Implementierung__

Basierend auf der Lösungsskizze ist die Gradoop Demo so zu erweitern, dass die Operatoren konfigurierbar 
und ausführbar sind. Hierbei ist darauf zu achten, dass die zur Verfügung gestellten Testdaten sowie
Parameter flexibel ausgetauscht werden können. Auf Benutzerfreundlichkeit bei der Wahl von 
Formularfeldern etc. ist zu achten. 

Als Programmiersprache für Backend und Web-Service wird Java 1.8 verwendet. Im Frontend JavaScript.

__3 - Vortrag__

Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze 
(kurze Beschreibung der Architektur, Nennung der verwendeten Bibliotheken) und die Funktionsweise der Anwendung
mittels einer Live-Demo vorgestellt werden. 


## Literatur

- [Evolution Analysis of Large Graphs with Gradoop](https://dbs.uni-leipzig.de/file/LEGECML-PKDD_2019_paper_9.pdf)
- [Temporal Graph Analysis using Gradoop](https://dbs.uni-leipzig.de/file/TemporalGraphAnalysisUsingGradoop-BTW2019.pdf)

## Links

- [Gradoop Github](https://github.com/dbs-leipzig/gradoop)
- [Gradoop - Temporal Graph Support (Github Wiki)](https://github.com/dbs-leipzig/gradoop/wiki/Temporal-Graph-Support)
- [Gradoop_demo Github](https://github.com/dbs-leipzig/gradoop_demo)
