# Big-Data Praktikum

## Übersicht


Das Praktikum beinhaltet den Entwurf und die Realisierung einer Anwendung oder eines Algorithmus im Big-Data-Umfeld. In der Regel erfolgt die Implementierung unter Verwendung eines Big-Data-Frameworks, wie z. B. Hadoop, Apache Spark, Apache Flink oder Gradoop erstellt werden soll. 

<!--break-->

Im einzelnen sind folgende Teilaufgaben zu lösen:

1. <b>Konzeptioneller Entwurf</b>. Es ist ein Entwurfsdokument anzufertigen, welches konzeptionell den Ablauf und die Architektur ihrer Anwendung darstellt. Im Dokument muss ersichtlich werden, welches Ziel verfolgt wird und welche Aufgaben dafür zu bewältigen sind. Das Dokument soll sich vom Umfang auf ca. 4 Seiten beschränken.
2. <b>Implementierung</b>. Basierend auf ihrem Entwurf soll die Anwendung realisiert werden. Das Resultat dieser Phase ist ein dokumentiertes, ausführbares Programm.
3. <b>Abschlusspräsentation</b> Am Ende des Praktikums stellt jede Gruppe ihr Projekt vor, wobei sie ihre Anwendung beschreibt sowie die Resultate präsentiert. Die Dauer der Präsentation soll ca. <b>10 Minuten</b> (+ 5 min. Diskussion) betragen.


## Anmeldung und Präsenzveranstaltungen


* Die Anmeldung zum Praktikum erfolgt über <a href="https://almaweb.uni-leipzig.de/" target = "_blank">Almaweb</a>. 
  * Bei Fragen und Problemen zur Anmeldung wenden Sie sich bitte immer an das Studienbüro via einschreibung(at)math.uni-leipzig.de

* Für die endgültige Anmeldung und Themenzuordnung melden Sie sich im Moodle unter folgender Adresse an:
<a href='https://moodle2.uni-leipzig.de/course/view.php?id=24647'> Anmeldung Moodle</a><br>
 Der Anmeldezeitraum ist vom 20.04-24.04. Dabei werden die Anmeldelisten aus dem AlmaWeb und der Anmeldung im Moodle abgeglichen, 
so dass Studenten, die angemeldet sind garantiert ein Thema bekommen.

* <b>Vorbesprechung</b>
  * Erste Mailkommunikation für die Konkretisierung des Themas und die ersten Schritte werden mit dem Betreuer per Mail abgehalten.
  


## Testate


Das Praktikum gliedert sich in drei Teile. Nach jeder der drei Teilaufgaben wird eine Dokumentation oder ein Testat per Skype/Slack/ oder ähnliches durchgeführt. 
Die Art der Durchführung soll mit dem Betreuer individuell abgesprochen werden.
Zum erfolgreichen Absolvieren des Praktikums müssen <b>alle</b> drei Testate erfolgreich abgelegt werden. 
Wird ein Termin nicht eingehalten, verfallen die bereits erbrachten Teilleistungen. 
Die konkreten Termine für die ersten zwei Testate bzw. Abgabe sind mit dem Betreuer per E-Mail zu vereinbaren. 
Es gelten die nachfolgenden Fristen:

* Testat 1 (Entwurf): Ende Mai
* Testat 2 (Realisierung): Mitte-Ende Juli
* Testat 3 (Präsentation): tba


## Themen

<strong class="aktuell">Bitte vereinbaren Sie zeitnah einen Termin mit Ihrem Betreuer zur Besprechung der ersten Schritte!</strong>


|Nr|Thema| Betreuer| Technologie| Studenten|
|--|-----|---------|------------|---------------------|
|01 | [Autoencoder für Bloom-Filter](autoencoder_pprl.md)| Christen| Python, Keras||
|02 | [Verteiler Datenstream: Optimierung](LID-DS-Stream.md)| Grimmer| tba||
|03 | [Bird Voice Recognition](bird_recognition.md) | Franke | Weka, MLlib, o.ä.|Richter/Schwerter| 
|04 | [COVID-19 Classification ](COVID_xray_Klassifikation.md)| Peukert&Martin| Python, Tensorflow, JavaScript/HTML, Docker |Beckert/Arkan|
|05 | [Temporal Graph Explorer](temporal_graph_explorer.md)| Rost| Java, JavaScript |Irmler/Kokemoor|
|06 | [FAMER Data Visualization](FAMER-Statistic-Visualizer.md)| Obraczka| Java, Flink, Python ||
|07 | [Erkennung von Hundebellen](dog_recognition.md) |Schneider| Python, Tensorflow|Matzeck/Panitz|
|08 | [Fashion Object Detection and Segmenation](fashion_object_detection_segmentation.md)  |Köpcke| Python, pytorch Tensorflow|Hakimi|
|09 | [Graph Stream Algorithmen](graph_stream_algorithms.md) | Gomez | Java, Flink |Lehmann/Koch|
|10 | [Fashion Feature Annotation](fashion_features.md) | Wilke | Python, Pytorch, Tensorflow, (Vue.js, flask)|Wuerf/Mäuer|
|11 | [Free Spoken Digits](free_spoken_digits.md) | Sehili | Python, Tensorflow|Staskewitsch/Ehrich|
|12 | [Plant Identification](plant_identification.md) | Lin | Python, PyTorch, o.ä.|Winter/Dolouei|

Teilnehmerkreis


Master-Studiengänge Data-Science und bei freien Plätzen auch Master-Studiengänge Informatik.  Die Teilnahme erfolgt in 2er-Gruppen, 
<strong class="aktuell">die Teilnehmerzahl ist auf ca. 22 Studierende beschränkt!</strong>. 
 Zu beachten ist, dass Studenten, die das Big-Data-Praktikum noch nicht belegt haben, bevorzugt werden. 

## Erwartete Vorkenntnisse


* **Java**-Kentnisse
* Vorlesung Cloud Data Management und NoSQL-Datenbanken hilfreich
* Linux-Kenntnisse von Vorteil
* Git-Kenntnisse von Vorteil
