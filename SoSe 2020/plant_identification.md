# Automated Plant Identification

## Motivation

The recent progress in deep learning has been proved to be beneficial to automated identification of plants. There exists some projects such as the [Pl@ntNet initiative](https://plantnet.org/en/) to help people to identify plant species based on photos taken by the users. A fully automatic and correct identification system would scale up the world-wide collection of plant observations and might provide valuable data for ecological studies. 

## Zielstellung

Following on the [PlantCLEF 2017 task](https://www.imageclef.org/lifeclef/2017/plant), the aim of the Praktikum is to develop automated methods that are able to identify the 742 plants of the test set correctly. 

## Arbeitspakete

### 1. Datenextraktion & Lösungsskizze
- Datenextraktion: The training data  of the PlantCLEF 2017 task is available at (https://www.imageclef.org/lifeclef/2017/plant). In addition to the given training data, further training sets can also be used if suitable sources are found.
- Lösungskizze: A search in the literature will help to find possible methods. For instance, the overview of the 2017 PlantCLEF results is a good starting point (http://ceur-ws.org/Vol-1866/invited_paper_9.pdf). However, not restricted to the methods used by the teams in the task, newer methods shall also be considered. 

### 2. Implementierung
- Based on the literature search, possible methods shall be implemented. Comparisons between different methods / workflows can also be considered. 

### 3. Evaluierung
- The evaluation of the identification shall use the same metrics used in the 2017 LifeCLEF task for easy direct comparison. The metrics are: Top1, Top3, Top5 accuracy and the Mean Reciprocal Rank (MRR).

### 4. Vortrag
- Ausarbeit einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze (kurze Beschreibung des Algorithmus, Nennung der verwendeten Bibliotheken) und die Ergebnisse der Evaluierung dargestellt werden. 

## Weiterführende Arbeiten

Since the PlantCLEF 2017 task was held few years ago, the further development in automated identification of plants since then might be able to achieve better results. Moreover, the addition of new training data might further improve the identification quality or it actually brings in more noise into the model. 

## Literatur

- [Pl@ntNet initiative](https://plantnet.org/en/)
- [PlantCLEF 2017 task](https://www.imageclef.org/lifeclef/2017/plant)
- [Training data sets of PlantCLEF 2017](http://otmedia.lirmm.fr/LifeCLEF/PlantCLEF2017/TrainPackages/)
- [Test data set](http://otmedia.lirmm.fr/LifeCLEF/PlantCLEF2017/TestPackage/PlantCLEF2017Test.tar.gz)
- [Plant identification based on noisy web data: the amazing performance of deep learning (LifeCLEF 2017)](http://ceur-ws.org/Vol-1866/invited_paper_9.pdf)