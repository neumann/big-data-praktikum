# Fashion Image Feature Annotation

## Motivation

Um verschiedene Angebote im E-commerce miteinander vergleichen zu können muss automatisch entschieden werden ob es sich hierbei um das gleiche Produkt handelt. Hierbei werden Methoden des Entity Matching verwendet, welche auf dem Vergleich zweier Einträge in einer Tabelle oder zweier Beschreibungstexte beruhen. Im Fashion Bereich können diese Texte oder Features sehr unterschiedlich ausfallen. Zum Glück gibt es jedoch meistens aussgekräftige Abbildungen des Produktes, aus welchen fehlende Beschreibungen generiert werden können.


## Zielstellung

Es muss ein geeigneter Datensatz mit vorhandenen Annotation ausgewählt werden, bspw. [Kaggle: Fashion Product Images Dataset](https://www.kaggle.com/paramaggarwal/fashion-product-images-dataset#styles.csv) oder [Fashionpedia](https://fashionpedia.github.io/home/Fashionpedia_download.html). Mittels dieses Datensatzes soll ein Model trainiert werden, welches aus Bildern von Kleidungsstücken sinnvolle Features generiert (Farbe, Typ, ...)

## Arbeitspakete

### 1. Datenextraktion, -analyse & Lösungsskizze
Zunächst soll sich ein Überblick über die vorhandenen Datensätze verschafft werden, eventuell können sie vereinigt werden um einen größeren zu schaffen. Der geeigneteste Datensatz soll ausgewählt werden und muss hinsichtlich der Feautures und ihrer Verteilung analysiert werden. Daraufhin kann das weitere Vorgehen in einer Skizze geplant werden.

### 2. Modellbildung
Basierend auf der Lösungsskizze sind die ML-Verfahren und die Anwendung zu implementieren. 
Die Lösungsskizze ist entsprechend zu aktualisieren.
Alle genutzten Quellen - auf für Codefragmente - sind zu nennen!

### 3. Evaluierung
 Das umgesetzte Verfahren ist hinsichtlich der Genauigkeit zu beurteilen. Precision und Recall bieten sich hier als Metriken an. Es kann untersucht werden ob einzelne Eigenschaften leichter zu extrahieren sind als andere. 

### 4. Vortrag
Ausarbeit einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze (kurze Beschreibung des Algorithmus/Architektur, Anwendung, sowie Nennung der verwendeten Bibliotheken) und die Ergebnisse der Evaluierung dargestellt werden. 



