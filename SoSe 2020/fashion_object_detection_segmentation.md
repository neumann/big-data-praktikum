# Fashion Object Detection and Segmentation

## Motivation

Wo kann ich das coole Outfit kaufen? Wir sehen coole Kleider, Schuhe oder Taschen, fotografieren sie und sofort zeigt einem eine Fashion App, wo wir sie kaufen können
Wir wollen KI-Methoden anwenden, um automatisch Kleidungsstücke in Fotos zu erkennen und anschließend den entsprechenden Bereich ausschneiden zu können. 

![](https://github.com/switchablenorms/DeepFashion2/blob/master/images/pair.jpg)

## Zielstellung

Für die Aufgabe soll der DeepFashion2 Benchmark (https://github.com/switchablenorms/DeepFashion2) verwendet werden. Der Benchmark enthält für eine Vielzahl von unterschiedlichen Kleidungsstücken Aufnahmen aus verschiedenen Blickwinkeln. 
Dazu sind verschiedene Arbeitsschritte notwendig. 
Zunächst müssen die Daten geladen und strukturiert werden. Für die Entwicklung eines Modells müssen die Kleidungsstücke identifiziert und anschließend segmentiert werden. 

## Arbeitspakete

### 1. Datenextraktion & Lösungsskizze


### 2. Modellbildung
- Basierend auf der Lösungsskizze sind die ML-Verfahren und die Anwendung zu implementieren. 
Die Lösungsskizze ist entsprechend zu aktualisieren.
Alle genutzten Quellen - auf für Codefragmente - sind zu nennen!

### 3. Evaluierung
- Das umgesetzte Verfahren ist hinsichtlich der Genauigkeit zu beurteilen. Für die Bewertung der Objekterkennung eignet sich die bounding box's average precision

### 4. Vortrag
- Ausarbeit einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze (kurze Beschreibung des Algorithmus/Architektur, Anwendung, sowie Nennung der verwendeten Bibliotheken) und die Ergebnisse der Evaluierung dargestellt werden. 

## Weiterführende Arbeiten

 

## Darüber hinaus


## Literatur
