# Dimensionsreduktion mit Autoencoders am Anwendungsfall 3D-Druck 

## Motivation
In der additiven Fertigung, insbesondere bei der Herstellung metallischer Bauteile kommt das 
Selective-Laser-Melting (SLM) zum Einsatz. Beim SLM wird ein Metallpulverbett mithilfe eines Lasers 
schichtweise aufgeschmolzen und eine dreidimensionale Fertigungsaufgabe auf zwei Dimensionen 
reduziert. Die Grundlage für SLM stellt ein CAD-Modell dar, welches das Bauteil im Vorfeld digital in 
dünne Scheiben aufteilt. Es besteht somit die Möglichkeit, auch individuell komplexe, geometrische 
Fertigungsgegenstände herzustellen. Zum aktuellen Zeitpunkt werden verschiedene Komponenten 
und Strukturen der Bauteile durch Trial-and-Error optimiert. Dies hat zur Folge, dass Bauteile erst 
nach dem Vollenden des gesamten Bauprozesses auf ihre Bauteilqualität untersucht werden können. 
Dazu werden die Bauteile durch verschiedene Experimente auf Zug -und Druckfestigkeit geprüft, da 
dies ausschlaggebende Faktoren für die Bauteilqualität sind. Dieser Prozess ist sehr kostenintensiv, 
zeitaufwendig und im Allgemeinen sehr ineffizient. Mithilfe von Machine Learning Methoden können 
Sensordaten schon während der Fertigung analysiert und ausgewertet werden. Auf diese Weise 
lassen sich Rückschlüsse, hinsichtlich relevanter Prozesseigenschaften und Produktqualitätseinflüsse 
ableiten.  

## Zielstellung  
Anhand von Bilddaten einzelner Layer (9 verschiedener Metallbauteile) sollen Merkmale von 
Bauteilen charakterisiert und mögliche Korrelationen und Anomalien zwischen den Bauteilen 
aufgezeigt werden. Dabei sollen Autoencoders zum Einsatz kommen, mit denen die Bilddaten 
zunächst in einen niedrigdimensionalen Vektorraum projiziert werden. Auf Grundlage dieser 
projizierten Code Vektoren ist ein Clustering über den Fertigungszeitraum durchzuführen.  

## Aufgaben

### 1. Theoretische Grundlagen und Lösungsskizze: 

Im ersten Schritt sollen die theoretischen Grundlagen eines Autoencoders erarbeitet und verstanden 
werden. Weiterhin sollen Implementierungsmöglichkeiten eines Autoencoders mithilfe von 
vorhanden Python Bibliotheken recherchiert werden. Hinsichtlich der Bilddaten ist eine 
Vorverarbeitung notwendig, bei der die jeweiligen Bereiche der Bauteile zu spezifizieren sind. 

### 2. Implementierung: 

Auf Basis der Lösungsskizze ist ein Autoencoder selbstständig zu implementieren und auf den 
gegebenen Datensatz „Components“ anzuwenden. 

Evaluierung und Visualisierung: 
Die implementierten Funktionen sind auf dem Datensatz „Components“ zu evaluieren. Korrelationen 
und Anomalien zwischen den Bauteilen sollen visualisiert und durch empirische Analysen 
verdeutlicht werden. Ebenso wie das unüberwachte Lernen mit Autoencoders. 

### 3. Vortrag: 
Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze 
(Beschreibung der Verfahren, Anwendung, sowie Nennung der verwendeten Bibliotheken) und die 
Ergebnisse der Evaluierung dargestellt werden. 
