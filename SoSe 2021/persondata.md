# Extraktion und Matching von Personendaten aus Online-Datenquellen

## Motivation

Zur Evaluierung von Record-Linkage-Verfahren werden realistische Datensätze benötigt, um die Praxistauglichkeit der Verfahren einzuschätzen und verschiedene Verfahren miteinander zu vergleichen. Besonders beim Privacy-preserving Record Linkage (PPRL) mangelt es an geeigneten Datensätzen, da das Linkage hier vorrangig auf sensiblen Personendaten durchgeführt wird. Bisher werden vor allem Wählerverzeichnisse aus US-Bundesstaaten zur Evaluierung genutzt. Ziel der Arbeit ist es, weitere Datensätze zu erschließen, indem Personendaten aus Online-Datenquellen, wie z.B. Wikipedia, extrahiert werden.

## Zielstellung

Ziel des Themas ist die Entwicklung einer Anwendung zur Extraktion und Aufbereitung von Personendaten aus Online-Datenquellen.
Nach Extraktion der Daten sind Duplikate, d.h. Datensätze, welche sich auf diesselbe Person beziehen, mittels eines geeigneten Record-Linkage-Verfahrens zu ermitteln. Hierbei gibt es zwei wesentliche Herausforderungen: (1) Die Datensätze können Inkonsistenzen und Fehler aufweisen, z.B. veraltete oder fehlende Werte. Derartige Fehler müssen vom Record-Linkage-Verfahren berücksichtigt werden, indem ein Ähnlichkeitswert berechnet wird. Hierdurch kann später vom Benutzer eingestellt werden, bis zu welchem Ähnlichkeitswert (potentielle) Duplikate in den Datensatz inkludiert werden sollen. (2) Skalierbarkeit: Das Record-Linkage-Verfahren muss auf mehreren Datenquellen mit insgesamt mehreren Millionen Datensätzen anwendbar sein. Hierzu ist ein geeignetes Blocking-Verfahren zu realisieren, welches die Anzahl an Kandidatenpaaren und damit die notwendigen Vergleiche reduziert.

Der gesamte Prozess (Extraktion, Aufbereitung, Linkage) muss reproduzierbar und auf neue Datenquellen erweiterbar sein. Hierdurch soll es möglich sein, den erstellten Datensatz um neue oder aktualisierte Datensätze zu erweitern.


## Arbeitspakete

__1 - Anwendungsdesign und Lösungsskizze__

Zunächst sind geeignete Datenquellen mit Personendaten zu identifizieren und sich mit deren APIs vertraut zu machen. Darauf aufbauend ist eine Anwendung zur Extraktion und Aufbereitung der Daten zu konzipieren. Für das Linkage der Daten ist es notwendig, sich mit dem Record-Linkage-Prozess sowie typischen Verfahren vertraut zu machen. Basierend darauf sind geeignete Verfahren auszuwählen, um dann einen Record-Linkage-Prozess zu definieren.


__2 - Implementierung__

Basierend auf der Lösungsskizze ist die Anwendung prototypisch zu implementieren. Hierbei ist darauf zu achten, die Anwendung flexibel zu konzipieren, sodass die genutzten Datenquellen sowie das Record-Linkage-Verfahren ausgetauscht werden können. Als Programmiersprache wird Java verwendet. 


__3 - Vortrag__

Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze (kurze Beschreibung der Architektur, Grundidee der Verfahren) und die Ergebnisse vorgestellt werden. 


## Literatur & Links

- [WikiData](https://www.wikidata.org/wiki/Wikidata:Main_Page)
- [IMDB](https://www.imdb.com/interfaces/)
- [MusicBrainz](https://musicbrainz.org/doc/MusicBrainz_Database/Download)
- [DBLP](https://dblp.org/faq/1474679.html)



