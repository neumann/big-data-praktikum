# Big-Data Praktikum

## Übersicht


Das Praktikum beinhaltet den Entwurf und die Realisierung einer Anwendung oder eines Algorithmus im Big-Data-Umfeld. In der Regel erfolgt die Implementierung unter Verwendung eines Big-Data-Frameworks, wie z. B. Hadoop, Apache Spark, Apache Flink oder Gradoop erstellt werden soll. 

<!--break-->

Im einzelnen sind folgende Teilaufgaben zu lösen:

1. <b>Konzeptioneller Entwurf</b>. Es ist ein Entwurfsdokument anzufertigen, welches konzeptionell den Ablauf und die Architektur ihrer Anwendung darstellt. Im Dokument muss ersichtlich werden, welches Ziel verfolgt wird und welche Aufgaben dafür zu bewältigen sind. Das Dokument soll sich vom Umfang auf ca. 4 Seiten beschränken.
2. <b>Implementierung</b>. Basierend auf ihrem Entwurf soll die Anwendung realisiert werden. Das Resultat dieser Phase ist ein dokumentiertes, ausführbares Programm.
3. <b>Abschlusspräsentation</b> Am Ende des Praktikums stellt jede Gruppe ihr Projekt vor, wobei sie ihre Anwendung beschreibt sowie die Resultate präsentiert. Die Dauer der Präsentation soll ca. <b>10 Minuten</b> (+ 5 min. Diskussion) betragen.


## Anmeldung und Präsenzveranstaltungen


* Die Anmeldung zum Praktikum erfolgt über <a href="https://almaweb.uni-leipzig.de/" target = "_blank">Almaweb</a>. 
  * Bei Fragen und Problemen zur Anmeldung wenden Sie sich bitte immer an das Studienbüro via einschreibung(at)math.uni-leipzig.de

* Für die endgültige Anmeldung und Themenzuordnung melden Sie sich im Moodle unter folgender Adresse an:
<a href='https://moodle2.uni-leipzig.de/course/view.php?id=24647'> Anmeldung Moodle</a><br>
 Der Anmeldezeitraum ist vom 20.04-24.04. Dabei werden die Anmeldelisten aus dem AlmaWeb und der Anmeldung im Moodle abgeglichen, 
so dass Studenten, die angemeldet sind garantiert ein Thema bekommen.

* <b>Vorbesprechung</b>
  * Erste Mailkommunikation für die Konkretisierung des Themas und die ersten Schritte werden mit dem Betreuer per Mail abgehalten.
  


## Testate


Das Praktikum gliedert sich in drei Teile. Nach jeder der drei Teilaufgaben wird eine Dokumentation oder ein Testat per Skype/Slack/ oder ähnliches durchgeführt. 
Die Art der Durchführung soll mit dem Betreuer individuell abgesprochen werden.
Zum erfolgreichen Absolvieren des Praktikums müssen <b>alle</b> drei Testate erfolgreich abgelegt werden. 
Wird ein Termin nicht eingehalten, verfallen die bereits erbrachten Teilleistungen. 
Die konkreten Termine für die ersten zwei Testate bzw. Abgabe sind mit dem Betreuer per E-Mail zu vereinbaren. 
Es gelten die nachfolgenden Fristen:

* Testat 1 (Entwurf): Ende Mai
* Testat 2 (Realisierung): Mitte-Ende Juli
* Testat 3 (Präsentation): tba


## Themen

<strong class="aktuell">Bitte vereinbaren Sie zeitnah einen Termin mit Ihrem Betreuer zur Besprechung der ersten Schritte!</strong>


| Nr | Thema | Betreuer | Technologie| Studenten |
| -- |-------|----------|------------|-----------|
| 01 | Implementierung von Privacy-preserving Record Linkage ohne Linkage Unit | Franke/Rohde/Sehili | Java | -- |
| 02 | [Extraktion und Matching von Personendaten aus Online-Datenquellen (WikiData, IMDB, MusicBrainz, ...)](persondata.md)| Franke | Java | -- |
| 03 | Optimierung verteilter Graphanalyse mittels Graphpartitionierung  | Gomez | Java, Flink | --|
| 04 | [Gruppierung eines Graph Streams](04_graph_stream.md) | Rost | Java, Flink | -- |
| 05 | [Schnellere Übertragung von Corona-Fallzahlen mittels privater Blockchain](05_corona_blockchain.md) | Kreusch | Java, Hyperledger Fabric | -- |
| 06 | [Klassifikation von Unfallschäden bei PKWs mittels Deep Learning (in Kooperation mit Adesso)](06_car_damage.md) | Adameit/Hildebrand | Python | -- |
| 07 | [Building an AI-Integrated Software System for Code Suggestions in VSCode](ai_code_suggest.md) | Peukert/Sigmund | VSCode/C++ | -- |
| 08 | [Membership Inference Attack auf Aneurysma-Ruptur-Detektionsklassifizierer](membership_inference_attack.md)| Rohde/Schneider| Python | -- |
| 09 | [Iterative degree-based sampling of knowledge graphs for benchmark data creation](kg_degree_based_sampling.md)| Obraczka | Java, Flink | |
| 10 | Klassifikation von Bilddaten aus der Radiologie | Martin | Python | |
| 11 | [Attribute extraction from eCommerce product descriptions](11_attribute_extraction.md) | Köpcke | Python | |
| 12 |[Demonstrator Predictive Maintenance](demo_pred_main.md) | Burghardt | Python | |


Teilnehmerkreis


Master-Studiengänge Data-Science und bei freien Plätzen auch Master-Studiengänge Informatik.  Die Teilnahme erfolgt in 2er-Gruppen, 
<strong class="aktuell">die Teilnehmerzahl ist auf ca. 22 Studierende beschränkt!</strong>. 
 Zu beachten ist, dass Studenten, die das Big-Data-Praktikum noch nicht belegt haben, bevorzugt werden. 

## Erwartete Vorkenntnisse


* **Java**-Kenntnisse
* Vorlesung Cloud Data Management und NoSQL-Datenbanken hilfreich
* Linux-Kenntnisse von Vorteil
* Git-Kenntnisse von Vorteil
