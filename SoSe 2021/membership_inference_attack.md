# Membership Inference Attack auf Aneurysma-Ruptur-Detektionsklassifizierer

## Motivation

Ein Aneurysma ist eine Anomalie in Blutgefäßen, das mit zunehmender Größe die Gefahr einer Ruptur birgt. Ein Riss eines Aneurysmas im Gehirn ist besonders kritisch, da dies zu schweren Schädigungen des Gehirns oder sogar zum Tod führen kann. Als Behandlung kommt eine Operation in Frage, die aber selbst sehr riskant ist. Maschinelles Lernen kann Ärzten hier helfen, die Wahrscheinlichkeit einer Ruptur auf Basis der Gesundheitsdaten des Patienten vorherzusagen und so möglicherweise unnötige Operationsrisiken zu vermeiden.

Basierend auf einem realen Datensatz mit klinischen Daten von 400 Patienten, die an Aneurysmen im Gehirn leiden, soll ein Machine Learning-Klassifikator konstruiert werden, um eine mögliche Aneurysma-Ruptur zu erkennen. Die Daten beinhalten 20-30 verschiedene klinische Merkmale, z.B. Geschlecht, Alter, Herkunft und Rauchgewohnheiten.

Da diese Daten sehr persönliche Informationen über die Patienten enthalten, ist es notwendig, dass beim Trainieren solcher Klassifikationsmodelle die Privatsphäre der Patienten nicht gefährdet wird. Das Gebiet des Privacy Preserving Machine Learning (PPML) erforscht Methoden, um eine hohe Klassifizierungsgenauigkeit unter Wahrung der Privatsphäre der Trainingsdaten zu erreichen. Dazu ist es notwendig, das Angriffsrisiko eines Modells zu bewerten, um die Wirksamkeit der getroffenen Maßnahmen zu evaluieren. In diesem Projekt soll ein solcher Angriff durchgeführt werden.

## Zielstellung

Die Aufgabe des Projektes ist es, einen Klassifikator zur Vorhersage der Rupturwahrscheinlichkeit von Aneurysmen auf Basis klinischer Merkmale zu konstruieren und anschließend die Angreifbarkeit des trainierten Modells hinsichtlich eines Leaks privater Informationen aus den Trainingsdaten zu untersuchen. Dazu soll eine Membership Inference Attack durchgeführt werden. Mit diesem Angriff lässt sich herausfinden, ob eine Person Teil der Trainingsdatenmenge eines Klassifikators war.

## Arbeitspakete

### 1. Anwendungsdesign und Lösungsskizze
Zunächst ist es notwendig, sich mit dem Datensatz vertraut zu machen.
Anschließend soll ein für das Problem geeignetes Machine-Learning-Modell ausgewählt sowie eine passende ML-Bibliothek gefunden werden, in welcher der entsprechende Algorithmus auf Basis der Daten trainiert und dessen Klassifikationsgenauigkeit evaluiert werden kann. Da der Fokus des Projektes vorrangig auf der Untersuchung der Privatsphäre und der Machbarkeit der Attacke liegen soll, werden Empfehlungen für Modell und Training des Klassifikators bereitgestellt.

Es ist eine Lösungsskizze angefertigen, wie eine Membership Inference Attack auf das Modell durchgeführt und der Erfolg evaluiert werden kann.

### 2. Implementierung und Evaluierung
In einem ersten Schritt soll das auf den Originaldaten trainierte Modell mit einer Membership Inference Attack angegriffen werden. Dazu bietet es sich an, ein Framework wie Privacy Raven zu verwenden. Es soll hierbei bewertet werden, wie effizient der Angriff durchgeführt werden kann.
Anhand eines zweiten, aus den Originaldaten abgeleiteten, synthetischen und privatisierten Datensatzes soll anschließend untersucht werden, ob der Angriff auch auf das darauf trainierte Modell erfolgreich durchgeführt werden kann.
Zusätzlich oder alternativ kann auch ein eigenes Modell mit dem DP-SGD-Algorithmus (siehe auch Literatur und Links unten) trainiert und die Machbarkeit des Angriffs überprüft werden.

### 3. Vortrag
Ausarbeit einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze (kurze Beschreibung des Algorithmus und Angriffsmodels, Nennung der verwendeten Bibliotheken) und die Ergebnisse des Angriffs dargestellt werden. 


## Literatur
- Shokri et al (2017): Membership Inference Attacks against Machine Learning Models
- Triastcyn, Aleksei & Faltings, Boi. (2018): Generating Differentially Private Datasets Using GANs
- Abada et al (2016): Deep Learning with Differential Privacy

## Links
- https://github.com/trailofbits/PrivacyRaven
- https://github.com/stratosphereips/awesome-ml-privacy-attacks
- https://github.com/tensorflow/privacy
- https://medium.com/pytorch/differential-privacy-series-part-1-dp-sgd-algorithm-explained-12512c3959a3
