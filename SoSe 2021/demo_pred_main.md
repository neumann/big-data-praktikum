# Demonstrator Predictive Maintenance

## Motivation
Predictive Maintenance beruht auf der Analyse historischer Anlagedaten mit statistischen Methoden und 
maschinellen Lernverfahren. Die Modelle werden für die Vorhersage des zukünftigen Maschinenverhaltens 
eingesetzt und helfen dabei, kritische Anlagenzustände rechtzeitig zu erkennen und geeignete Maßnahmen zu 
ergreifen.

![Predictive Maintenance](images/PredMaintenance.png)

# Demonstrator
![Demonstrator](images/PredMaintenanceDemo.png)

## Aufbau
![Aufbau](images/Workflow.png)


# Aufgaben

1. Erfassung von Maschinendaten
 Initial soll die Zielstellung erarbeitet werden. Dies umfasst die Planung der Messreihen sowie die Erzeugung der Daten.

2. Vorhersage von Maschinenzuständen
Für die Erstellung eines Modells müssen die Daten abgerufen werden und gefiltert werden. Weiterhin sind die Daten zu bereinigen, um qualitative Trainingsdaten zu erhalten. Basierend auf den Daten kann dann ein Modell für die Vorhersage von Zuständen traininiert und genutzt werden.

3. Präsentation
Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung und die Lösungsskizze beschrieben werden.


